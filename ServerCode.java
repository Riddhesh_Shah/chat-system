import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class ServerCode {

    static HashMap<Integer, ServerCodeOutput> clientPorts = new HashMap<Integer, ServerCodeOutput>();
    static int id = 0;

    public static void main(String[] args) {

        while (true){

            try(ServerSocket serverSocket = new ServerSocket(5000)){

                Socket socket = serverSocket.accept();
                System.out.println("Client Connected!" + "\n ID - " + id + " Port - " + socket.getLocalPort());

                ServerCodeInput input = new ServerCodeInput(socket);
                input.start();

                ServerCodeOutput output = new ServerCodeOutput(socket);
                output.start();

                clientPorts.put(id++,output);

            }catch(Exception e){
                System.out.println(e);
            }

        }

    }

}
