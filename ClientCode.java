import java.io.IOException;
import java.net.Socket;

public class ClientCode {

    static String name;

    public static void main(String[] args) {

        String echoString = "";

        try(Socket socket = new Socket("localhost", 5000)){

            ClientCodeInput input = new ClientCodeInput(socket);
            input.start();

            ClientCodeOutput output = new ClientCodeOutput(socket, input);
            output.start();

            while (input.isAlive() && output.isAlive());

        }catch(IOException ie){
            System.out.println(ie);
        }

    }

}
