import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerCodeOutput extends Thread{

    String serverMessage;
    Socket socket;
    PrintWriter output;

    ServerCodeOutput(Socket socket){
        this.socket = socket;
    }

    public void run(){

        try{

            output = new PrintWriter(socket.getOutputStream(),true);

            while (true){

                if("exit".equalsIgnoreCase(serverMessage)){
                    break;
                }

            }

        } catch(Exception e){
            System.out.println(e);
        } finally{

            try{
                socket.close();
            }catch(IOException ie){
                System.out.println(ie);
            }

        }

    }

    public void messagePasser(String message){

        serverMessage = message;
        System.out.println("By: "+message);
        output.println(message);

    }

}
