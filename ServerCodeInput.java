import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerCodeInput extends Thread{

    String clientMessage;
    Socket socket;

    ServerCodeInput(Socket socket){
        this.socket = socket;
    }

    public void run(){

        try{

            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while (true){

                clientMessage = input.readLine();

                if(clientMessage != null){

                    System.out.println("Sent by client: " + clientMessage);

                    String[] clientSplitter = clientMessage.split(":");
                    int id = Integer.parseInt(clientSplitter[0]);

                    ServerCode.clientPorts.get(id).messagePasser(clientSplitter[1]);

                    if("exit".equalsIgnoreCase(clientMessage)) {
                        break;
                    }

                }

            }

        }catch (IOException ie){

            System.out.println(ie);

        }finally {

            try{
                socket.close();
            }catch (IOException ie){
                System.out.println(ie);
            }

        }

    }

}
