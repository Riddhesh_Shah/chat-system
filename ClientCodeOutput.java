import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientCodeOutput extends Thread{

    String echoString;
    Socket socket;
    ClientCodeInput input;

    ClientCodeOutput(Socket socket, ClientCodeInput input){
        this.socket = socket;
        this.input = input;
    }

    public void run(){

        try{

            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            Scanner scanner = new Scanner(System.in);

            while (true){

                System.out.println("Enter text you want to sent to client\n" + "(id:message)");
                echoString = scanner.nextLine();
                output.println(echoString);

                if("exit".equalsIgnoreCase(echoString)){
                    break;
                }

            }

        } catch(IOException ie){
            System.out.println(ie);
        } finally{

            try{
                socket.close();
            }catch(IOException ie){
                System.out.println(ie);
            }

        }

    }

}
