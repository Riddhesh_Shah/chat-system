import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientCodeInput extends Thread{

    String response;
    Socket socket;

    ClientCodeInput(Socket socket){
        this.socket = socket;
    }

    public void run(){

        try{

            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while (true){

                response = input.readLine();
                System.out.println("From Client: " + response);

                if("exit".equalsIgnoreCase(response)){
                    break;
                }

            }

        }catch(IOException ie){
            System.out.println(ie);
        } finally{

            try{
                socket.close();
            }catch(IOException ie){
                System.out.println(ie);
            }

        }

    }

}
